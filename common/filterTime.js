function filterTime(val) {
	// '2019-04-06 11:37',
	let d = new Date(val);
	let y = d.getFullYear();
	let m = d.getMonth() + 1 // 月
	m = m < 10 ? "0" + m : m;
	let da = d.getDate();
	da = da < 10 ? "0" + da : da;
	let h = d.getHours();
	let mi = d.getMinutes(); // 分
	mi = mi < 10 ? "0" + mi : mi;
	return `${y}-${m}-${da} ${h}:${mi}`;
}

export default filterTime;
