export default function(){
	return new Promise((resolve,reject)=>{
		uni.showLoading({
			title:"登录中……"
		});
		
		uni.login({
			provider: 'weixin',
			success(e) {
				if (e.code) {
					uniCloud.callFunction({ 
						name: 'loginMP',
						data: {
							code:e.code
						}
					}).then(res=>{
						uni.hideLoading()
						if (res.result.status !== 0) {
							return Promise.reject(new Error(res.result.msg))
						}
						// 登录成功 返回 token,openid
						resolve(res)
					}).catch(err=>{
						uni.hideLoading()
						reject(err)
					})
				} else {//获取登录code失败
					uni.hideLoading()
					reject(new Error('微信登录失败'))
				}
			},//获取登录code失败
			fail(e) {
				uni.hideLoading()
				reject(new Error('微信登录失败'))
			}
		})
	})
};
