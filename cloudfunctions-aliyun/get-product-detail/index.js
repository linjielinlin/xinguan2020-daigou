/**
 * @description  获取商品详情
 * @author unhejing (成都-敬宇杰)
 * @date 2020-02-21 上午18:13:07
 */
'use strict';
const db = uniCloud.database()
exports.main = async (event, context) => {
  //event为客户端上传的参数
  	console.log('event:' + event)
	// event.id = "007abbbe-11ba-438e-a4e7-2441085784d8"
  	if(event.id == '' || !event.id){
  		return {
  			success: false,
  			code: -1,
  			msg: '参数错误，缺少商品id'
  		}
  	}
  	console.log('event.id:' + event.id)
  	let res = await db.collection('product').where({
  			'_id': event.id,
  		}).get()
  	if (!res.data || res.data.length === 0) {
  		return {
  			success: false,
  			code: -1,
  			msg: '暂无数据'
  		}
  	}
	let product = res.data[0];
	// 获取图片信息
	let imgArr = await getProductImg(event.id);
	product.otherImgs = imgArr;
	console.log("商品详情响应数据:"+JSON.stringify(product))
  	if (res.id || res.affectedDocs >= 1) {
  		return {
  			success: true,
  			code: 0,
  			msg: '成功',
  			data: product
  		}
  	}
  	return {
  		success: false,
  		code: -1,
  		msg: '服务器内部错误'
  	}
};

// 获取商品图片
async function getProductImg(product_id) {
	let res = await db.collection('product_img').where({
		'product_id': product_id
	}).get();
	if (res.id || res.affectedDocs >= 1){
		return res.data;
	}else{
		return [];
	}
}
